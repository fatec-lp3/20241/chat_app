// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class SignInPage extends StatelessWidget {
  final _auth = FirebaseAuth.instance; // Singleton
  final _txtEmail = TextEditingController();
  final _txtPassword = TextEditingController();

  SignInPage({super.key});

  void _signIn(BuildContext context) async {
    try {
      await _auth.signInWithEmailAndPassword(
        email: _txtEmail.text,
        password: _txtPassword.text,
      );

      Navigator.pushReplacementNamed(context, '/messages');

    } on FirebaseAuthException catch (ex) {

      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text(ex.message!), ),
      );
      
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          height: 200,
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              TextField(
                controller: _txtEmail,
                decoration: InputDecoration(
                    hintText: "E-mail", border: OutlineInputBorder()),
              ),
              TextField(
                controller: _txtPassword,
                obscureText: true,
                decoration: InputDecoration(
                    hintText: "Password", border: OutlineInputBorder()),
              ),
              ElevatedButton(
                child: Text("SignIn"),
                onPressed: () => _signIn(context),
              ),
              TextButton(
                child: Text("New User"),
                onPressed: () => Navigator.pushNamed(context, '/signup'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
