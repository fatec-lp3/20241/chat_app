import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

import 'chatapp.dart';

const options = FirebaseOptions(
  apiKey: "AIzaSyBKJzao81MajTsINsvKfRbwxIBXR2DEnxA",
  authDomain: "chatapp-e0b23.firebaseapp.com",
  projectId: "chatapp-e0b23",
  storageBucket: "chatapp-e0b23.appspot.com",
  messagingSenderId: "303242862710",
  appId: "1:303242862710:web:2a2f27aee6670f1269ff57"
);

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: options);
  runApp(const ChatApp());
}