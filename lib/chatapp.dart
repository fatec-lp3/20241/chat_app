import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'signin.page.dart';
import 'signup.page.dart';
import 'messages.page.dart';

class ChatApp extends StatelessWidget {
  const ChatApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        useMaterial3: false,
      ),
      routes: {
        "/signin":(context) => SignInPage(),
        "/signup":(context) => SignUpPage(),
        "/messages":(context) => MessagesPage(),
      },
      initialRoute: FirebaseAuth.instance.currentUser == null ? '/signin' : '/messages',
    );
  }
}